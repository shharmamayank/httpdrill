const http = require('http');
const {v4 : uuidv4} = require('uuid');
const fs = require('fs');


const port = process.env.port || 3000;

const server = http.createServer((request, response) => {

    const statusCode = request.url.split('/')[2];
    const delayInSeconds = request.url.split('/')[2];

    if (request.url === "/html") {

        response.writeHead(200, { 'Content-Type': 'text/html' })
        fs.readFile('./index.html', (error, data) => {
            if (error) {
                response.writeHead(404);
                response.write("Error : File not found");

            } else {
                response.write(data);
            }
            response.end();
        })
    } else if (request.url === '/json') {

        response.writeHead(200, { 'Content-Type': 'application/json' });
        fs.readFile("data.json", (error, data) => {

            if (error) {
                response.writeHead(404);
                response.write("Error : File not found");
            } else {
                response.write(data);
            }
            response.end();

        })
    } else if (request.url === '/uuid') {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        const uuidObject = {
            uuid: uuidv4()
        };
        response.write(JSON.stringify(uuidObject));
        response.end();

    } else if (request.url === `/status/${statusCode}`) {

        try {
            response.writeHead(statusCode, { 'Content-Type': 'application/json' });
            const statusMessage = {
                status: `${statusCode} ${http.STATUS_CODES[statusCode]}`
            };
            response.write(JSON.stringify(statusMessage));
            response.end();
        } catch {
            response.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "Error status code"
            };
            response.write(JSON.stringify(errorMessage));
            response.end();
        }
    } else if (request.url === `/delay/${delayInSeconds}`) {

        const seconds = Number(delayInSeconds);
        if (isNaN(seconds) || seconds < 0) {
            response.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "NAN"
            }
            response.write(JSON.stringify(errorMessage));
            response.end();
        }
        else {
            setTimeout(() => {
                response.writeHead(200, { 'Content-Type': 'application/json' });
                const delayTimes = {
                    'delayInseconds': seconds
                };
                response.write(JSON.stringify(delayTimes));
                response.end();
            }, seconds * 1000);
        }
    } else {
        response.writeHead(404, { 'Content-Type': 'application/json' });
        const errorMessage = {
            message: "Not found"
        }
        response.write(JSON.stringify(errorMessage));
        response.end();
    }

});

server.listen(port, (error) => {

    if (error) {
        console.log("Somethings wrong with the server!!");
    } else {
        console.log("Server is listening on port  " + port);
    }

});
